# Education system

## About the project
This education system is made up of `courses`, `teachers`, `students`, and `books`.  
Upon startup, if there is a file of existing data found, you are asked whether or not to import it.  

---

## Install
First install make:  
`$sudo apt install make`  
To build, from the root folder use:  
`$ make`  

---

## Usage  
To run, use:  
`$ make run`  
Once started, simply answer the prompts on screen.  
When given different options to choose from, simply write them into the console.  

---

## Sources
Serialization is done using hps library which can be found here:  
https://github.com/jl2922/hps
