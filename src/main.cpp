#include "school.h"
#include "file_handler.h"
#include "printer.h"
#include "group.h"
#include <iostream>
#include "input_receiver.h"
#include <fstream>

// Translate chars to integers such that they are easier used in the switch/case statements
enum command_types {COURSE='c', GROUP='g', STUDENT='s', TEACHER='t', BOOK='b', LIBRARY='l', ADD='a', REMOVE='r', JOIN='j', EXIT_EDUCATION='e', PRINT='p'};
const std::string datafilename = "school.bin";


void add(School& school) {
    // LEVEL 2 MENU FOR ADDING THINGS
    char command = Input::get_char("What to add?\nOptions: [c]ourse / [t]eacher / [s]tudent / [b]ook", false, true);
    switch (command)
    {
        case STUDENT:
            school.add_student();
            break;
        case TEACHER:
            school.add_teacher();
            break;
        case COURSE:
            school.add_course();
            break;
        case BOOK:
            school.get_library().add_book();
        default:
            break;
    }
}

void print(School& school) {
    // LEVEL 2 MENU FOR PRINTING THINGS
    char command = Input::get_char("What to print info about?\nOptions: [s]tudent / [t]eacher / [c]ourse / [b]ook / [l]ibrary", false, true);
    switch (command)
    {
        case COURSE: {
            std::string coursename = Input::get_string("Please enter name of course:", false, true);
            if (school.has_course(coursename)) {
                Printer::print_course(school.get_course(coursename));
            } else {
                std::cout << "Course " << coursename << " does not exist!" << std::endl;
            }
            break;}

        case TEACHER: {
            int teacher_id = Input::get_int("Please enter id of teacher:", false, true);
            if (school.has_teacher(teacher_id)) {
                Printer::print_teacher(school.get_teacher(teacher_id));
            } else {
                std::cout << "This teacher does not exist!" << std::endl;
            }
            break;}

        case STUDENT: {
            int student_id = Input::get_int("Please enter id of student:", false, true);
            if (school.has_student(student_id)) {
                Printer::print_student(school.get_student(student_id));
            } else {
                std::cout << "This student does not exist!" << std::endl;
            }
            break;}

        case BOOK: {
            std::string bookname = Input::get_string("Please enter name of book:", false, true);
            if (school.get_library().has_book(bookname)) {
                Printer::print_book(school.get_library().get_book(bookname));
            } else {
                std::cout << "Could not find any books with title '" << bookname << "'!" << std::endl;
            }
            break;}
        
        case LIBRARY: {
            Printer::print_library(school.get_library());
            break;}
        
        default:
            std::cout << "Invalid command" << std::endl;
            break;
    }

}

void join(School& school) {
    // LEVEL 2 MENU FOR JOINING STUDENTS AND TEACHERS TO CLASSES
    char command = Input::get_char("Who should join a course?\nOptions: [s]tudent / [t]eacher", false, true);
    switch (command)
    {
        case STUDENT:
            school.enroll_student();
            break;

        case TEACHER:
            school.set_teacher();
            break;
        
        default:
            break;
    }
}

void remove(School& school) {
    // LEVEL 2 MENU FOR REMOVING THINGS
    char command = Input::get_char("Who/what should be removed?\nOptions: [s]tudent / [t]eacher / [b]ook", false, true);

    switch (command)
    {
    case STUDENT:
        school.remove_student();
        break;

    case TEACHER:
        school.remove_teacher();
        break;
    
    case BOOK: {
        std::string booktitle = Input::get_string("Please enter the book title: ");
        if (school.get_library().has_book(booktitle)) {
            school.get_library().remove_book(booktitle);
            std::cout << "Successfully removed " << booktitle << "from the library!" << std::endl;
        } else {
            std::cout << "This book does not exist!" << std::endl;
        }
        break;}
    
    default:
        break;
    }

}

int main(int argc, char const *argv[])
{
    School school;
    char command;

    // READ FILE AND PARSE DEPENDING ON USERS DECISION
    Filehandler::load(school, datafilename, true);


    // LEVEL 1 MENU
    while (1)
    {
        command = Input::get_char("What would you like to do?\nOptions: [a]dd / [p]rint / [j]oin / [r]emove / [e]xit", false, true);

        switch (command)
        {
        case ADD:
            add(school);
            break;

        case PRINT:
            print(school);
            break;

        case JOIN:
            join(school);
            break;

        case REMOVE:
            remove(school);
            break;

        case EXIT_EDUCATION:
            Filehandler::save(school, datafilename);
            return 0;
            break;

        default:
            break;
        }

    }
    return 0;
}
