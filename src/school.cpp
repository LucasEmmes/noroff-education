#include <iostream>
#include "school.h"
#include "humans.h"
#include <algorithm>
#include "input_receiver.h"

School::School() {};

// Getters
bool School::has_course(const std::string& coursename) const {
    return this->courses.find(coursename) != this->courses.end();
};

bool School::has_student(const int& id) const {
    return this->students.find(id) != this->students.end();
}

bool School::has_teacher(const int& id) const {
    return this->teachers.find(id) != this->teachers.end();
}

Group& School::get_course(const std::string& coursename) {
    return this->courses[coursename];
}


Student& School::get_student(const int& id) {
    return this->students[id];
}


Teacher& School::get_teacher(const int& id) {
    return this->teachers[id];
}

Library& School::get_library() {
    return this->library;
}


// Setters

void School::add_student() {
    std::cout << "Please enter the following information:" << std::endl;
    
    std::string name = Input::get_string("Name:", true);
    int age = Input::get_int("Age:", true);

    Student student(name, age);
    this->students[student.get_personal_id()] = student;
    std::cout << "Successfully added student #" << student.get_personal_id() << std::endl;
    std::cout << std::endl;
}

void School::add_teacher() {
    std::cout << "Please enter the following information:" << std::endl;
    
    std::string name = Input::get_string("Name:", true);
    int age = Input::get_int("Age:", true);
    int salary = Input::get_int("Salary:", true);

    Teacher teacher(name, age, salary);
    this->teachers[teacher.get_personal_id()] = teacher;
    std::cout << "Successfully added teacher #" << teacher.get_personal_id() << "\n" << std::endl;
}

void School::add_course() {
    std::cout << "Please enter the following information:" << std::endl;

    std::string name = Input::get_string("Name:", true);

    if (this->courses.find(name) == this->courses.end()) {
        Group course(name);
        this->courses[name] = course;
        std::cout << "Successfully added course '" << name << "'" << std::endl;
        std::cout << std::endl;
    }
}


void School::enroll_student() {
    int student_id = Input::get_int("Please enter student's id:", true);
    if (this->has_student(student_id)) {
        Student& student = this->get_student(student_id);

        std::string coursename = Input::get_string("What course to enroll:");
        if (this->has_course(coursename)) {
            Group& course = this->get_course(coursename);
            student.enroll_course(course.get_name());
            course.add_student(student.get_personal_id());

            std::cout << "Successfully added student #" << student_id << " to course " << coursename << "!" << std::endl;
        } else {
            std::cout << "Could not find any courses with that name!" << std::endl;

        }

    } else {
        std::cout << "Could not find any students with this id!" << std::endl;
    }
}

void School::set_teacher() {
    int teacher_id = Input::get_int("Please enter teacher's id:", true);
    if (this->has_teacher(teacher_id)) {
        Teacher& teacher = this->get_teacher(teacher_id);

        std::string coursename = Input::get_string("What course should they instruct:");
        if (this->has_course(coursename)) {
            Group& course = this->get_course(coursename);
            teacher.enroll_course(course.get_name());
            course.add_teacher(teacher.get_personal_id());

            std::cout << "Successfully added teacher #" << teacher_id << " to course " << coursename << "!" << std::endl;
        } else {
            std::cout << "Could not find any courses with that name!" << std::endl;
        }

    } else {
        std::cout << "Could not find any teachers with this id!" << std::endl;
    }
}

void School::remove_student() {
    int student_id = Input::get_int("Please enter student's id:", true);
    if (this->has_student(student_id)) {
        Student& student = this->get_student(student_id);

        std::string coursename = Input::get_string("What course to kick from: ");
        if (this->has_course(coursename)) {
            Group& course = this->get_course(coursename);
            if (student.attends_course(coursename)) {
                student.leave_course(coursename);
                course.remove_student(student_id);
            } else {
                std::cout << "This student does not attend " << coursename << "!" << std::endl;
            }
        } else {
            std::cout << "Could not find any courses with that name!" << std::endl;
        }

    } else {
        std::cout << "Could not find any students with this id!" << std::endl;
    }
}

void School::remove_teacher() {
    int teacher_id = Input::get_int("Please enter teacher's id:", true);
    if (this->has_teacher(teacher_id)) {
        Teacher& teacher = this->get_teacher(teacher_id);

        std::string coursename = Input::get_string("What course to kick from:");
        if (this->has_course(coursename)) {
            Group& course = this->get_course(coursename);
            if (teacher.attends_course(coursename)) {
                teacher.leave_course(coursename);
                course.remove_student(teacher_id);
            } else {
                std::cout << "This student does not attend " << coursename << "!" << std::endl;
            }
        } else {
            std::cout << "Could not find any courses with that name!" << std::endl;
        }

    } else {
        std::cout << "Could not find any teachers with this id!" << std::endl;
    }
}