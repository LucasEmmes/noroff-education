#include "group.h"
#include <algorithm>

Group::Group() {};

Group::Group(const std::string& group_name) {
    this->name = group_name;
};

// Getters

bool Group::is_in_group(const int& student_id) const {
    return std::find(students.begin(), students.end(), student_id) != students.end();
};

std::string Group::get_name() const {return this->name;};

int Group::get_teacher() const {return this->teacher;};

std::vector<int> Group::get_students() const {return this->students;};

// Setters

void Group::add_student(const int& student_id) {
    if (!is_in_group(student_id)) {
        students.push_back(student_id);
    }
};

void Group::remove_student(const int& student_id) {
    if (is_in_group(student_id)) {
        students.erase(std::find(students.begin(), students.end(), student_id));
    }    
};

void Group::add_teacher(const int& teacher_id) {
    teacher = teacher_id;
};

void Group::remove_teacher(const int& teacher_id) {
    teacher = 0;
};

