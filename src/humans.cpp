#include "humans.h"
#include <algorithm>

Person::Person() {};
Person::Person(const std::string& name, const int& age) {
    this->personal_id = ++next_personal_id;
    this->name = name;
    this->age = age;
};

// Getters
int Person::get_personal_id() const {return this->personal_id;};

std::string Person::get_name() const {return this->name;};

int Person::get_age() const {return this->age;};

std::vector<std::string> Person::get_courses() const {return this->courses;};

bool Person::attends_course(const std::string& course_name) const {
    return std::find(this->courses.begin(), this->courses.end(), course_name) != this->courses.end();
};

// Setters

void Person::enroll_course(const std::string& course_name) {
    this->courses.push_back(course_name);
}
void Person::leave_course(const std::string& course_name) {
    this->courses.erase(std::find(this->courses.begin(), this->courses.end(), course_name));
}



// STUDENT
Student::Student():Person() {};
Student::Student(const std::string& name, const int& age):Person(name, age) {this->school_year = age-5;};
// Student getters
int Student::get_school_year() const {return this->school_year;};


// TEACHER
Teacher::Teacher():Person() {};
Teacher::Teacher(const std::string& name, const int& age, const int& salary):Person(name, age) {this->salary = salary;};
// Teacher getters
int Teacher::get_salary() const {return this->salary;};