#pragma once
#include <string>
#include <vector>

class Group {

    private:
        std::string name;
        std::vector<int> students;
        int teacher = 0;

    public:
        Group();
        Group(const std::string& group_name);
        // Getters
        bool is_in_group(const int& student_id) const;
        std::string get_name() const;
        std::vector<int> get_students() const;
        int get_teacher() const;
        // Setters
        void add_student(const int& student_id);
        void remove_student(const int& student_id);
        void add_teacher(const int& teacher_id);
        void remove_teacher(const int& teacher_id);


    template<class Buffer>
    void serialize(Buffer& buffer) const {
        buffer << name << students << teacher;
    }
    
    template<class Buffer>
    void parse(Buffer& buffer) {
        buffer >> name >> students >> teacher;
    }
};