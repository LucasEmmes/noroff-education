#include "library_and_books.h"
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "input_receiver.h"

Book::Book() {};
Book::Book(const std::string& title, const std::string& author, const int& year, const int& number_of_pages) {
    this->title = title;
    this->author = author;
    this->year = year;
    this->number_of_pages = number_of_pages;
};


// Getters
bool Book::is_borrowed() const {return this->borrowed;};
std::string Book::get_title() const {return this->title;};
std::string Book::get_author() const {return this->author;};
int Book::get_year() const {return this->year;};
int Book::get_length() const {return this->number_of_pages;};

// Setters
void Book::borrow_book() {if (!borrowed) {borrowed = true;}};
void Book::return_book() {borrowed = false;};
void Book::borrow() {this->borrowed = true;}




Library::Library() {};
bool Library::has_book(const std::string& bookname) const {
    return this->books.find(bookname) != this->books.end();
};


// Getters
Book& Library::get_book(const std::string& bookname) {
    return this->books[bookname];
};

std::vector<std::string> Library::get_books() const {
    std::vector<std::string> titles;
    for (const auto& book : this->books)
        titles.push_back(book.first);
    return titles;
};


// Setters
void Library::add_book() {
    std::cout << "Please enter the following information:" << std::endl;

    std::string title = Input::get_string("Title:", true);
    std::string author = Input::get_string("Author:", true);
    int year = Input::get_int("Year: ", true);
    int length = Input::get_int("# of pages: ", true);

    if (this->has_book(title)) {
        std::cout << "This book already exists!\n" << std::endl;
        return;}

    Book book(title, author, year, length);
    this->books[title] = book;
    std::cout << "Successfully added book" << "\n" << std::endl;
};

void Library::remove_book(const std::string& booktitle) {
    books.erase(booktitle);
};

void Library::borrow_book(Book& book) {
    book.borrow();
};
