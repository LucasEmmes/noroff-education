#pragma once
#include <map>

#include "humans.h"
#include "group.h"
#include "library_and_books.h"

class School {

    private:
        std::map<int, Student> students;
        std::map<int, Teacher> teachers;
        std::map<std::string, Group> courses;
        Library library;

    public:
        School();
        // Getters
        bool has_course(const std::string& coursename) const;
        bool has_student(const int& id) const;
        bool has_teacher(const int& id) const;
        Group& get_course(const std::string& coursename);
        Student& get_student(const int& id);
        Teacher& get_teacher(const int& id);
        Library& get_library();
        // Setters
        void add_student();
        void add_teacher();
        void add_course();
        void enroll_student();
        void set_teacher();
        void remove_student();
        void remove_teacher();


    template<class Buffer>
    void serialize(Buffer& buffer) const {
        buffer << students << teachers << courses << library;
    }
    
    template<class Buffer>
    void parse(Buffer& buffer) {
        buffer >> students >> teachers >> courses >> library;
    }

};