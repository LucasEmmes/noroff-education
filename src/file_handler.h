#pragma once
#include <string>
#include "hps/hps.h"
#include <string>
#include <fstream>
#include "input_receiver.h"
#include "school.h"

class Filehandler
{

    public:
        template <class D>
        static void save(D &data, const std::string &filename)
        {
            std::ofstream datafile(filename, std::ios::binary); // Open file
            std::string data_string = hps::to_string(data);     // Serialize school object
            datafile << data_string;                            // Write data
            datafile.close();                                   // Close file
        };

        template <class D>
        static void load(D &data, const std::string &filename, bool prompt)
        {
            std::ifstream datafile(filename, std::ios::binary); // Open file
            if (datafile.is_open())
            {
                char wants_to_load = 'y';
                if (prompt)
                {
                    wants_to_load = Input::get_char("File from previous session was found.\nWould you like to load it? [y]es/[n]o", false, true);
                }
                if (wants_to_load == 'y')
                {
                    std::string data_string;
                    getline(datafile, data_string);               // Read data
                    data = hps::from_string<School>(data_string); // Parse data to school object
                }
            }
        };
        
};