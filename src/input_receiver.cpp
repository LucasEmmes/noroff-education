#include <iostream>
#include <string>
#include <limits>
#include "input_receiver.h"


// Pormpts the user and returns the first character of their answer (in lowercase)
char Input::get_char(const std::string& prompt, bool sameline, bool newline) {
    std::string response = Input::get_string(prompt, sameline, newline);
    return std::tolower(response[0]);
}

// Prompts the user and returns their answer parsed as an integer
int Input::get_int(const std::string& prompt, bool sameline, bool newline) {
    int response = std::stoi(Input::get_string(prompt, sameline, newline));
    return response;
}

// Pormpts the user and returns their answer as a string
std::string Input::get_string(const std::string& prompt, bool sameline, bool newline) {
    if (sameline) {
        std::cout << prompt << " ";
    } else {
        std::cout << prompt << "\n> ";
    }
    std::string reponse;
    std::getline(std::cin, reponse);
    if (newline) {
        std::cout << std::endl;
    }
    return reponse;    
}