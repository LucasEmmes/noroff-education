#pragma once
#include <string>
#include <vector>

class Person {

    protected:
        static inline int next_personal_id = 0;     // Generates new IDs such that no two people share an ID
        int personal_id;
        std::string name;
        int age;
        std::vector<std::string> courses;

    public:
        Person();
        Person(const std::string& name, const int& age);  
        // Getters
        int get_personal_id() const;
        std::string get_name() const;
        int get_age() const;
        std::vector<std::string> get_courses() const;
        // Setters
        void enroll_course(const std::string& course_name);
        void leave_course(const std::string& course_name);
        bool attends_course(const std::string& course_name) const;

    template<class Buffer>
    void serialize(Buffer& buffer) const {
        buffer << next_personal_id << personal_id << name << age << courses;
    }
    
    template<class Buffer>
    void parse(Buffer& buffer) {
        buffer >> next_personal_id >> personal_id >> name >> age >> courses;
    }
};



class Student : public Person {

    private:
        int school_year;

    public:
        Student();
        Student(const std::string& name, const int& age);
        //Getters
        int get_school_year() const;

    template<class Buffer>
    void serialize(Buffer& buffer) const {
        Person::serialize(buffer);
        buffer << school_year;
    }
    
    template<class Buffer>
    void parse(Buffer& buffer) {
        Person::parse(buffer);
        buffer >> school_year;
    }
};



class Teacher : public Person {

    private:
        int salary;

    public:
        Teacher();
        Teacher(const std::string& name, const int& age, const int& salary);
        // Getters
        int get_salary() const;

    template<class Buffer>
    void serialize(Buffer& buffer) const {
        Person::serialize(buffer);
        buffer << salary;
    }
    
    template<class Buffer>
    void parse(Buffer& buffer) {
        Person::parse(buffer);
        buffer >> salary;
    }
};