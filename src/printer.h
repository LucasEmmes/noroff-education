#pragma once
class Student;
class Teacher;
class Group;
class Book;
class Library;

class Printer {

    public:
        static void print_student(const Student& student);
        static void print_teacher(const Teacher& teacher);
        static void print_course(const Group& group);
        static void print_book(const Book& book);
        static void print_library(const Library& library);
        
};