#pragma once
#include <string>

class Input {

    public:
        static char get_char(const std::string& prompt, bool sameline = false, bool newline = false);
        static int get_int(const std::string& prompt, bool sameline = false, bool newline = false);
        static std::string get_string(const std::string& prompt, bool sameline = false, bool newline = false);
        
};