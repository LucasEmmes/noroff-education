#include "printer.h"
#include <iostream>
#include "humans.h"
#include "group.h"
#include "library_and_books.h"

void Printer::print_student(const Student& student) {
    std::cout << "ID: " << student.get_personal_id() << std::endl;  
    std::cout << "Name: " << student.get_name() << std::endl;
    std::cout << "Age: " << student.get_age() << std::endl;
    std::cout << "Grade: " << student.get_school_year() << std::endl;
    std::cout << "Courses: ";
    std::vector<std::string> courses = student.get_courses();
    for (std::string& coursename : courses)
        std::cout << coursename << " ";
    std::cout << std::endl;
    std::cout << std::endl;
};

void Printer::print_teacher(const Teacher& teacher) {
    std::cout << "ID: " << teacher.get_personal_id() << std::endl;  
    std::cout << "Name: " << teacher.get_name() << std::endl;
    std::cout << "Age: " << teacher.get_age() << std::endl;
    std::cout << "Salary: " << teacher.get_salary() << std::endl;
    std::cout << "Courses: ";
    std::vector<std::string> courses = teacher.get_courses();
    for (std::string& coursename : courses)
        std::cout << coursename << " ";
    std::cout << std::endl;
    std::cout << std::endl;
};

void Printer::print_course(const Group& group) {
    std::cout << "Name: " << group.get_name() << std::endl;
    std::cout << "Instructor: " << group.get_teacher() << std::endl;
    std::cout << "Students: ";
    std::vector<int> students = group.get_students();
    for (int& student : students)
        std::cout << student << " ";
    std::cout << std::endl;
    std::cout << std::endl;
};

void Printer::print_book(const Book& book) {
    std::cout << "Title: " << book.get_title() << std::endl;
    std::cout << "Author: " << book.get_author() << std::endl;
    std::cout << "Year published: " << book.get_year() << std::endl;
    std::cout << "Length: " << book.get_length() << " pages" << std::endl;
    std::cout << std::endl;
};

void Printer::print_library(const Library& library) {
    std::cout << "Books:" << std::endl;
    for (std::string& title : library.get_books())
        std::cout << "\t - " << title << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
};