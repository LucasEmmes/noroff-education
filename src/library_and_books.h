#pragma once
#include <string>
#include <map>
#include <vector>

class Book {

    private:
        std::string title;
        std::string author;
        int year;
        int number_of_pages;
        bool borrowed;

    public:
        Book();
        Book(const std::string& title, const std::string& author, const int& year, const int& number_of_pages);
        void borrow_book();
        void return_book();
        bool is_borrowed() const;
        std::string get_title() const;
        std::string get_author() const;
        int get_year() const;
        int get_length() const;
        void borrow();


    template<class Buffer>
    void serialize(Buffer& buffer) const {
        buffer << title << author << year << number_of_pages << borrowed;
    }

    template<class Buffer>
    void parse(Buffer& buffer) {
        buffer >> title >> author >> year >> number_of_pages >> borrowed;
    }
};



class Library {
    
    private:
        std::map<std::string, Book> books;
    
    public:
        Library();
        bool has_book(const std::string& bookname) const;
        Book& get_book(const std::string& bookname);
        void add_book();
        void remove_book(const std::string& booktitle);
        void borrow_book(Book& book);
        std::vector<std::string> get_books() const;

    template<class Buffer>
    void serialize(Buffer& buffer) const {
        buffer << books;
    }
    
    template<class Buffer>
    void parse(Buffer& buffer) {
        buffer >> books;
    }
};